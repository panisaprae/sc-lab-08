package Model2;

public class Product implements Taxable{
	private String ProductName;
	private double Price;
	
	public Product(String PProductName, double PPricee){
		ProductName = PProductName;
		Price = PPricee;
	}
	
	public String getName(){
		return ProductName;
	}
	
	public double getPrice(){
		return Price;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return Price*0.07;
	}
}
