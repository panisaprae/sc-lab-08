package Model2;

public class PersonT implements Taxable{
	private String Name;
	private double InCome;
	
	public PersonT(String NName, double IIncome){
		Name = NName;
		InCome = IIncome;
	}
	
	public String getName(){
		return Name;
	}
	
	public double getIncome(){
		return InCome;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double sum,sum1,result = 0;
		if (InCome > 0 && InCome <= 300000){
			sum = InCome*0.05;
			return sum;
		}else{
			sum = InCome-300000;
			sum1 = InCome-sum;
			result = (sum*0.10)+(sum1*0.05);
			return result;
		}
	}
}
