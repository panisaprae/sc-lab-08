package Model2;

import java.util.ArrayList;


public class TaxCalculator {
	
	public static double sum(ArrayList<Taxable> taxList){
		double sum = 0;
		for (Taxable obj : taxList) {
			sum = sum + obj.getTax(); 
		}
		return sum;
	}

}
