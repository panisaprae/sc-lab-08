package Model2;

import java.util.ArrayList;



public class TaxMain {
	
	public static void main(String[] args) {	
		
		ArrayList<Taxable> person = new ArrayList<Taxable>();
		person.add(new PersonT("Navaphob", 800000));
		person.add(new PersonT("Thanawat", 300000));
		person.add(new PersonT("Piyoungloon", 650000));
		
		double sumTaxPerson = TaxCalculator.sum(person);
		System.out.println("Sum Tax Person : " + sumTaxPerson);
		
		ArrayList<Taxable> company = new ArrayList<Taxable>();
		company.add(new Company("Speed Boat Services Company Limited", 7000000, 3000000));
		company.add(new Company("Chaiwat Television co.,Ltd", 1000000, 500000));
		company.add(new Company("Jarupha corporation public company limited", 40000000, 7000000));
		
		double sumTaxCompany = TaxCalculator.sum(company);
		System.out.println("Sum Tax Company : " + sumTaxCompany);
		
		ArrayList<Taxable> product = new ArrayList<Taxable>();
		product.add(new Product("Rotring", 199));
		product.add(new Product("Table", 2075));
		product.add(new Product("Paper A4", 560));
		
		double sumTaxProduct = TaxCalculator.sum(product);
		System.out.println("Sum Tax Product : " + sumTaxProduct);
	
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		tax.add(new PersonT("Saksit", 500000));
		tax.add(new Company("Wason Crop International co.,Ltd", 1000000, 800000));
		tax.add(new Product("Food", 100));

		double sumTax = TaxCalculator.sum(tax);
		System.out.println("Sum Tax : " + sumTax);
		
	}
}
