package Model2;

public class Company implements Taxable{
	private String Name;
	private double Recieve;
	private double Payment;
	
	public Company(String NName, double RReceive, double PPayment){
		Name = NName;
		Recieve = RReceive;
		Payment = PPayment;
	}
	
	public String getName(){
		return Name;
	}
	
	public double getReceive(){
		return Recieve;
	}
	
	public double getPayment(){
		return Payment;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double sum = 0;
		sum = (Recieve-Payment)*0.30;
		return sum;
	}
}
