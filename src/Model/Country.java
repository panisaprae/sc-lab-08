package Model;

public class Country implements Measurable{
	private String name;
	private double area;
	
	public Country(String NName, double AArea){
		name = NName;
		area = AArea;
	}
	
	public String getName(){
		return name;
	}
	
	public double getArea(){
		return area;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return area;
	}
}
