package Model;

public class BankAccount implements Measurable{
	private String name;
	private double balance;
	
	public BankAccount(String NName, double BBalance){
		name = NName;
		balance = BBalance;
	}
	
	public String getName(){
		return name;
	}
	
	public double getBalance(){
		return balance;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return 0;
	}

}
