package Model;


public class Main {

	public static void main(String[] args) {
		
	Measurable[] person = new Measurable[3];
	person[0] = new Person("Panisa", 160);
	person[1] = new Person("Jitra", 155);
	person[2] = new Person("Jainam", 178);
	
	double averageHeight = Data.getAverage(person);
	System.out.println("Average Height : " + averageHeight);
	

	Person Panisa = new Person("Panisa", 160);
	Person Jitra = new Person("Jitra", 152);
	Person Jainam = new Person("Jainam", 178);
		
	Measurable minheight = Data.Min(Panisa, Jitra, Jainam);
	Person per = (Person) minheight;
	double height = per.getHeight();
	System.out.println("Minimum Height : " + height);
	
	Country Jap = new Country("Japan", 9000000);
	Country Thai = new Country("Thailand", 7000000);
	Country Kore = new Country("Korea", 300000);
	
	Measurable mincountry = Data.Min(Jap, Thai, Kore);
	Country count = (Country) mincountry;
	double minarea = count.getArea();
	System.out.println("Minimum Area : " + minarea);
	
	BankAccount Fourbank = new BankAccount("Ratchaneeporn", 7200);
	BankAccount Oilbank = new BankAccount("Varicha", 3000);
	BankAccount Mintbank = new BankAccount("Narinrat", 2500);
	
	Measurable minbank = Data.Min(Fourbank, Oilbank, Mintbank);
	BankAccount bank = (BankAccount) minbank;
	double minBalance = bank.getBalance();
	System.out.println("Minimum Balance : " + minBalance);
	
	
	}
}
