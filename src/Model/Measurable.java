package Model;

public interface Measurable {
	double getMeasure();
}
