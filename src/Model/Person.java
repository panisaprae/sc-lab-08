package Model;

public class Person implements Measurable{
	private String name;
	private double height;
	
	public Person(String NName, double HHeight){
		name = NName;
		height = HHeight;
	}
	
	public String getName(){
		return name;
	}
	
	public double getHeight(){
		return height;
	}
	
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return height;
	}
}
